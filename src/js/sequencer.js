
import Reactor from './reactor.js'

import Notes from './notes.js'
import Oscillator from './oscillator.js'

// #TODO
// - Drag select step nodes.

const Sequencer = (sequencerName, context, masterGain) => {

	let name = sequencerName
	let nodes = []

	let pitchBase = 60 // base pitch/note number of sequencer
	let pitchModifier = 0

	let rows = 13 // all notes of octave
	let stepsLength = 16
	let speed = 1 

	function createGrid(amount = (rows * stepsLength)) {

		const sequencer = document.createElement('div')
		sequencer.classList.add(`sequencer`, this.name)

		let row = 1
		let step = 1

		let pitch = rows-1 // the pitch of highest node

		for (var i = 0; i < amount; i++) {

			// Create node element

			const stepEl = document.createElement('div')
			stepEl.classList.add('node')
			// stepEl.setAttribute('data-step', step)
			// stepEl.setAttribute('data-row', row)
			// stepEl.setAttribute('data-pitch', pitch)

			// Sync node with a simple reactive/observable object.

			const node = Reactor ({
				el: stepEl, 
				number: i+1,
				step: step,
				pitch: pitch,
				selected: false,
				active: false,
				onChange: function(key, value) {
					if(this.selected)
						this.el.classList.add('is-selected')
					else
						this.el.classList.remove('is-selected')
					if(this.active)
						this.el.classList.add('is-active')
					else
						this.el.classList.remove('is-active')
				},
			})

			// Calculate row, pitch, step.

			row = (step % stepsLength == 0) ? row + 1 : row
			pitch = (step % stepsLength == 0) ? pitch - 1 : pitch

			step = (step >= stepsLength) ? 1 : step + 1

			// Push nodes to array and append to parent

			sequencer.appendChild(stepEl)
			this.nodes.push(node.data)

			// Add node event listeners

			node.data.el.addEventListener('click', (event) => {
				node.data.selected = !node.data.selected
			})

			// node.data.el.addEventListener('dragenter', (event) => {
			// 	event.preventDefault()
			// })

			// node.data.el.addEventListener('drop', (event) => {
			// 	node.data.selected = !node.data.selected
			// })

			// node.data.el.addEventListener('dragover', (event) => {
			// 	event.preventDefault()
			// })

		}

		// Append sequencer to body

		document.body.appendChild(sequencer)

	}

	// Call update on step event.

	function update(currentStep) {

		const index = currentStep - 1

		// Play selected node

		this.nodes.forEach((node) => {
			if(node.step === currentStep && node.selected)
				this.playNode(node)
		})

		// Change step active class

		this.nodes.forEach((node) => node.active = false )
		for (var i = 0; i < rows; i++)
			this.nodes[index+(i*stepsLength)].active = true

	}

	// Play selected node in pattern.

	function playNode(node) {

		const osc = Oscillator(context, masterGain).init()

		osc.playNote({ 
			frequency: Notes().getFrequencyByNoteNumber( node.pitch + this.getPitch() )  
		})

	}

	function getPitch() {
		return this.pitchBase + this.pitchModifier.data.value
	}

	function onStop() {
		this.nodes.forEach((node) => node.active = false )
	}

	function clearSequence() {
		this.nodes.forEach((node) => node.selected = false)
	}

	function changePitch(pitchModifier) {
		this.pitchModifier.data.value = pitchModifier
		if(this.pitchModifier.data.value > 48)
			this.pitchModifier.data.value = 48
		if(this.pitchModifier.data.value < -48)
			this.pitchModifier.data.value = -48
	}

	function createHTML() {

		// Sequencer panel

		const sequencerPanel = document.createElement('div')
		sequencerPanel.classList.add('sequencer-panel')

			// Sequencer title

			const sequencerLabel = document.createElement('div')
			sequencerLabel.classList.add('ui-title')
			sequencerLabel.innerHTML = 'Sequencer'


			// Clear Sequence Button

			const clearSequenceButton = document.createElement('div')
			clearSequenceButton.classList.add('clear-sequence-button', 'ui-button', 'ui-button-smaller')
			clearSequenceButton.innerHTML = 'Clear Sequence'
			clearSequenceButton.addEventListener('click', this.clearSequence.bind(this))

			// Sequencer Pitch Label

			const pitchLabel = document.createElement('div')
			pitchLabel.classList.add('ui-title', 'ui-title-smaller')
			pitchLabel.innerHTML = 'Pitch'

			// Pitch Increment Button

			const pitchIncrementButton = document.createElement('div')
			pitchIncrementButton.classList.add('ui-button', 'ui-button-smaller')
			pitchIncrementButton.innerHTML = '+'

			pitchIncrementButton.addEventListener('click', () => {
				this.changePitch(this.pitchModifier.data.value+1)
			})

			// Pitch Decrement Button

			const pitchDecrementButton = document.createElement('div')
			pitchDecrementButton.classList.add('ui-button', 'ui-button-smaller')
			pitchDecrementButton.innerHTML = '-'

			pitchDecrementButton.addEventListener('click', () => {
				this.changePitch(this.pitchModifier.data.value-1)
			})

			// Pitch Value Label

			const pitchValueLabel = document.createElement('div')
			pitchValueLabel.classList.add('ui-node', 'ui-node-smaller')
			pitchValueLabel.innerHTML = this.pitchModifier

			this.pitchModifier = Reactor ({
				el: pitchValueLabel, 
				value: 0,
				onChange: function(key, value) {
					this.el.innerHTML = this.value
				},
			})

			// Append items to panel

			sequencerPanel.appendChild(sequencerLabel)
			sequencerPanel.appendChild(pitchLabel)
			sequencerPanel.appendChild(pitchIncrementButton)
			sequencerPanel.appendChild(pitchValueLabel)
			sequencerPanel.appendChild(pitchDecrementButton)
			sequencerPanel.appendChild(clearSequenceButton)

			// Append panel

			document.body.appendChild(sequencerPanel)

		// Create sequencer grid

		this.createGrid()

	}

	return {
		nodes,
		name,
		playNode,
		createGrid,
		update,
		clearSequence,
		createHTML,
		pitchBase,
		pitchModifier,
		changePitch,
		getPitch,
		onStop,
	}

}

export default Sequencer

