const Notes = (state) => {

	const middleA = 69
	let tuning = (state && state.tuning) ? state.tuning : 440

	let bottomNote = 12
	let topNote = 120

	const noteNames = [
		'C-',
		'C#',
		'D-',
		'D#',
		'E-',
		'F-',
		'F#',
		'G-',
		'G#',
		'A-',
		'A#',
		'B-',
	]

	let notes = []

	let currentNote = ''

	const createNotes = () => {

		// Create notes in our selected range.
		for (var noteNumber = bottomNote; noteNumber <= topNote; noteNumber++) {

			// Create note.
			const note = {
				id: noteNumber,
				name: getNoteNameByNoteNumber(noteNumber) + getOctaveByNoteNumber(noteNumber),
				frequency: getFrequencyByNoteNumber(noteNumber),
			}

			notes.push(note)

		}

	}

	const getOctaveByNoteNumber = (noteNumber) => {
		return Math.floor((noteNumber / 12) - 1)
	}

	const getFrequencyByNoteNumber = (noteNumber) => {
		return tuning * Math.pow(2, (noteNumber - middleA) / 12) // #TODO: tuning to Sampler.
	}

	const getNoteNameByNoteNumber = (noteNumber) => {
		return noteNames[noteNumber % 12]
	}

	// HTML

	const createNotesHTML = () => {

		const container = document.createElement('div')
		container.classList.add('notes')

		const selectedNote = document.createElement('div')
		selectedNote.classList.add('note', 'selected-note')

		let html = ''

		notes.forEach((n) => {

			if(n.id >= 48 && n.id <= 60) {

				const template = `
					<div class="note note-${n.id}" data-id="${n.id}" data-frequency="${n.frequency}" draggable="true">${n.name}</div>
				`

				html += template

			}

		})

		container.innerHTML = html

		document.body.appendChild(selectedNote)
		document.body.appendChild(container)

	}

	createNotes()

	return {
		currentNote,
		notes,
		getOctaveByNoteNumber,
		getFrequencyByNoteNumber,
		getNoteNameByNoteNumber,
		createNotesHTML,
	}

}

export default Notes

