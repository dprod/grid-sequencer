
const Chords = () => {

	// http://scottdavies.net/chords_and_scales/music.html

	let selectedChord = ''

	const chords = [
		{
			name: 'maj',
			notes: [1,5,8],
		},
		{
			name: 'min',
			notes: [1,4,8],	
		},
		{
			name: '7',
			notes: [1,5,8,11],
		},
		{
			name: 'min7',
			notes: [1,4,8,11],
		},
		{
			name: 'sus4',
			notes: [1,6,8],
		},
		{
			name: '7sus4',
			notes: [1,6,8,11],
		},
		{
			name: '6',
			notes: [1,5,8,10],
		},
		{
			name: 'min6',
			notes: [1,4,8,10],
		},
		{
			name: 'dim',
			notes: [1,4,7],
		},
	]

	function selectChord(chordButton) {
		console.log(`selectChord`, chordButton)
	}

	function createHTML() {

		const container = document.createElement('div')
		container.classList.add('chords')

		this.chords.forEach((chord) => {

			const chordButton = document.createElement('button')
			chordButton.classList.add('chord-button')
			chordButton.innerHTML = chord.name

			chordButton.addEventListener('click', this.selectChord)

			container.appendChild(chordButton)

		})

		document.body.appendChild(container)

	}

	function getChordByName(name) {
		return this.chords.find((chord) => chord.name === name)
	}

	return {
		chords,
		getChordByName,
		selectChord,
		createHTML,
	}

}

export default Chords