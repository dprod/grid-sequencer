
import Reactor from './reactor.js'

const Transport = (Clock) => {

	let isPlaying = false

	let stepCounter = null
	let tempo = null

	document.addEventListener('keydown', (event) => {

		if(event.keyCode === 32) {

			event.preventDefault()

			if(!Clock.isRunning) {
				Clock.startClock()
			}
			else {
				Clock.stopClock()
			}

		}

	})

	function createHTML() {

		// Transport

		const container = document.createElement('div')
		container.classList.add('transport')

		// Play Button

		const playButton = document.createElement('div')
		playButton.classList.add('play-button', 'ui-button')
		playButton.innerHTML = 'Play'

		const play = Reactor ({
			el: playButton, 
			name: 'play-button',
			active: false,
			onChange: function(key, value) {
				if(this.active)
					this.el.classList.add('is-active')
				else
					this.el.classList.remove('is-active')
			},
		})

		play.data.el.addEventListener('click', () => {
			this.isPlaying = true
			Clock.startClock()
		})

		// Stop Button

		const stopButton = document.createElement('div')
		stopButton.classList.add('stop-button', 'ui-button')
		stopButton.innerHTML = 'Stop'

		const stop = Reactor ({
			el: stopButton, 
			name: 'stop-button',
			active: false,
			onChange: function(key, value) {
				if(this.active)
					this.el.classList.add('is-active')
				else
					this.el.classList.remove('is-active')
			},
		})

		stop.data.el.addEventListener('click', () => {
			this.isPlaying = false
			Clock.stopClock()
		})

		// Step Counter

		this.stepCounter = document.createElement('div')
		this.stepCounter.classList.add('step-counter', 'ui-node')

		// Tempo

		this.tempo = document.createElement('input')
		this.tempo.classList.add('tempo', 'ui-input')

		this.tempo.addEventListener('focus', (event) => {

		})

		this.tempo.addEventListener('change', (event) => {
			if(event.target.value > 500)
				event.target.value = 500
			else if(event.target.value < 20)
				event.target.value = 20
			Clock.changeTempo(event.target.value)
		})

		this.tempo.addEventListener('keydown', (event) => {
			if(event.keyCode === 13)
				event.target.blur()
		})

		// Append items

		container.appendChild(playButton)
		container.appendChild(stopButton)
		container.appendChild(this.tempo)
		container.appendChild(this.stepCounter)

		// Append container

		document.body.appendChild(container)

	}

	function update({ currentStep, currentBeat, currentBar, tempo }) {
		this.stepCounter.innerHTML = `${currentBeat}.${currentStep}` // Use only beat and steps for now.
		if(tempo && this.tempo.value != tempo)
			this.tempo.value = tempo
	}

	return {
		isPlaying,
		createHTML,
		stepCounter,
		update,
	}

}

export default Transport
