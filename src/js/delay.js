
import Fader from './fader.js'

let Delay = (context, masterGain, delayTime = 0.2, feedbackValue = 0.7) => ({

	// waapi nodes
	delay: context.createDelay(),
	feedback: context.createGain(),
	gain: context.createGain(),
	filter: context.createBiquadFilter(),

	// Values
	delayTime: delayTime,
	feedbackValue: feedbackValue,

	// Faders
	delayTimeFader: null,
	feedbackValueFader: null,
	gainFader: null,

	create() {

		this.delay.delayTime.value = this.delayTime
		this.feedback.gain.value = this.feedbackValue

		this.delay.connect(this.feedback)
		this.feedback.connect(this.delay)

		this.delay.connect(this.filter)
		this.filter.connect(this.gain)

		this.filter.frequency.value = 2000
		this.filter.Q.value = 0
		this.gain.gain.value = 0.5

		this.createHTML()

		return this

	},

	createHTML() {

		// Delay container

		const container = document.createElement('div')
		container.classList.add('delay')
		document.body.appendChild(container)

			// Delay main label

			const delayLabel = document.createElement('div')
			delayLabel.classList.add('delay-label', 'ui-title')
			delayLabel.innerHTML = 'Delay'
			container.appendChild(delayLabel)

			// Fader label group

			const faderLabelGroup  = document.createElement('div')
			faderLabelGroup.classList.add('delay-fader-labels')
			container.appendChild(faderLabelGroup)

				// Delay time label

				const delayTimeLabel = document.createElement('div')
				delayTimeLabel.classList.add('ui-label', 'ui-label-small')
				delayTimeLabel.innerHTML = 'Time'
				faderLabelGroup.appendChild(delayTimeLabel)

				// Feedback label

				const feedbackLabel = document.createElement('div')
				feedbackLabel.classList.add('ui-label', 'ui-label-small')
				feedbackLabel.innerHTML = 'Feedback'
				faderLabelGroup.appendChild(feedbackLabel)

				// Delay gain label

				const delayGainLabel = document.createElement('div')
				delayGainLabel.classList.add('ui-label', 'ui-label-small')
				delayGainLabel.innerHTML = 'Gain'
				faderLabelGroup.appendChild(delayGainLabel)

			// Fader group

			const faderGroup = document.createElement('div')
			faderGroup.classList.add('delay-faders')
			container.appendChild(faderGroup)

				this.delayTimeFader = 

					Fader({ 
						minValue: 0.00, 
						maxValue: 1.00, 
						value: 0.2
					})
					.create({ 
						parentEl: faderGroup, 
					})
					.onUpdate((value) => {
						this.delay.delayTime.value = value
					})

				this.feedbackValueFader = 

					Fader({
						minValue: 0.00, 
						maxValue: 0.85, 
						value: 0.5
					})
					.create({ 
						parentEl: faderGroup, 
					})
					.onUpdate((value) => {
						this.feedback.gain.value = value
					})

				this.gainFader =  

					Fader({
						minValue: 0.00, 
						maxValue: 0.6, 
						value: 0.5
					})
					.create({ 
						parentEl: faderGroup, 
					})
					.onUpdate((value) => {
						this.gain.gain.value = value
					})

		// HTML Range inputs... not used, wanted to try to make my own faders.

		/*
		// Delay time fader

		const delayTimeFader = document.createElement('input')
		delayTimeFader.setAttribute('type', 'range')
		delayTimeFader.setAttribute('min', 0.01)
		delayTimeFader.setAttribute('max', 1.00)
		delayTimeFader.setAttribute('step', 0.01)

		delayTimeFader.value = this.delayTime

		delayTimeFader.addEventListener('input', (event) => {
			this.delay.delayTime.value = event.target.value
		})

		container.appendChild(delayTimeFader)

		// Feedback value fader

		const feedbackFader = document.createElement('input')
		feedbackFader.setAttribute('type', 'range')
		feedbackFader.setAttribute('min', 0.1)
		feedbackFader.setAttribute('max', 0.85)
		feedbackFader.setAttribute('step', 0.01)

		feedbackFader.value = this.feedbackValue

		feedbackFader.addEventListener('input', (event) => {
			this.feedback.gain.value = event.target.value
		})

		container.appendChild(feedbackFader)

		// Delay gain fader

		const delayGain = document.createElement('input')
		delayGain.setAttribute('type', 'range')
		delayGain.setAttribute('min', 0.0)
		delayGain.setAttribute('max', 0.6)
		delayGain.setAttribute('step', 0.01)

		delayGain.value = this.gain.gain.value

		delayGain.addEventListener('input', (event) => {
			this.gain.gain.value = event.target.value
		})

		container.appendChild(delayGain)
		*/

		// Append to body



	},


})


export default Delay