

import Delay from './delay.js'

// CONTEXT
try {
	window.AudioContext = window.AudioContext || window.webkitAudioContext
} catch (e) {
	window.alert('Your browser has no Web Audio API support...')
}

const context = new window.AudioContext()

// MASTER GAIN
const masterGain = context.createGain()
masterGain.gain.value = 0.5
masterGain.name = 'master-gain'

// ANALYSER
const analyser = context.createAnalyser()

// MASTER DELAY
const masterDelay = Delay(context).create()

// OUTPUT BUS / GAIN
const outputGain = context.createGain()


/*
    ------------------------------------------------
    | • OSC ->                                     |
    |    • MASTER GAIN ->                          |
    |    • DELAY -> FEEDBACK -> (REVERB) ->        |
    |       • ANALYSER ->                          |
    |          • OUTPUT GAIN ->                    |
    |             • CONTEXT DESTINATION            |
    ------------------------------------------------
*/

masterGain.connect(analyser)
masterGain.connect(masterDelay.delay)
masterDelay.gain.connect(analyser)
analyser.connect(outputGain)
outputGain.connect(context.destination)

export { context, masterGain, analyser }

