
import waaclock from 'waaclock'

const Clock = ({context, onStepCallback, onStopCallback}) => {

	let clock = new waaclock(context, { toleranceEarly: 0.1 })

	let tempo = 120
	let signature = 4

	let stepDuration = (60 / tempo) / 2

	let stepEvent = null

	let currentStep = 1
	let currentBeat = 1
	let currentBar = 1

	let isRunning = false

	//////////////////////////////////////////////////////////////////////////////// 

	function startClock() {

		if(this.isRunning)
			return

		this.isRunning = true

		// #TODO
		this.stepDuration = (60 / this.tempo) / 2

		this.clock.start()

		this.stepEvent = this.clock.callbackAtTime(this.onStepEvent.bind(this), context.currentTime)
			.repeat(this.stepDuration)
			.tolerance({ early: 0.01, late: 0.01 })

	}

	function stopClock() {

		if(!this.isRunning)
			return

		this.currentStep = 1
		this.currentBeat = 1
		this.currentBar = 1
		this.isRunning = false

		this.clock.stop()
		onStopCallback()

	}

	function changeTempo(tempo) {
		this.tempo = tempo
		// this.stepDuration = (60 / this.tempo) / 2
		if(this.isRunning) {
			this.stopClock()
			this.startClock()
		}
	}

	function onStepEvent() {

		onStepCallback({
			currentStep: this.currentStep, 
			currentBeat: this.currentBeat,
			currentBar: this.currentBar,
			tempo: this.tempo,
		})

		this.updateTime()

	}

	function nextStepTime() {
		
	}

	function updateTime() {

		this.currentStep = (this.currentStep >= 16) ? 1 : this.currentStep + 1

		this.currentBeat = (this.currentStep % 4 == 1) ? this.currentBeat + 1 : this.currentBeat
		this.currentBeat = (this.currentBeat > 4) ? 1 : this.currentBeat

		this.currentBar = (this.currentStep == 1) ? this.currentBar + 1 : this.currentBar
		this.currentBar = (this.currentBar > 4) ? 1 : this.currentBar

	}

	return {
		clock,
		currentStep,
		currentBeat,
		currentBar,
		isRunning,
		startClock,
		stopClock,
		tempo,
		changeTempo,
		onStepEvent,
		updateTime,
	}


}

export default Clock