
const Oscillator = (context, masterGain, note) => {

	let oscillator = context.createOscillator()

	let gain = context.createGain()

	let panner = context.createStereoPanner()

	function playNote(note) {

		// #TODO: Clean up

		// const randomNumber = (Math.floor(Math.random() * 7) + 1)
		const randomNumber = 0
		const relativePitchDrift = (randomNumber / note.frequency) * 100

		// Set frequency with slight random pitch drift.
		this.oscillator.frequency.setValueAtTime(note.frequency + relativePitchDrift, context.currentTime)

		// Set random pan
		const randomPan = (((Math.floor(Math.random() * 40) + 1))-10) / 100
		this.panner.pan.value = randomPan

		// Set attack & decay
		const attackTime = 0.02
		const decayTime = 1.0 + (randomNumber / 100)

		this.gain.gain.setValueAtTime(0.0000001, context.currentTime)
		this.gain.gain.exponentialRampToValueAtTime(0.5, context.currentTime + attackTime)
		this.gain.gain.exponentialRampToValueAtTime(0.0000001, context.currentTime + decayTime + attackTime)
		this.oscillator.stop(context.currentTime + decayTime + attackTime)

		this.oscillator.onended = () => {
			this.gain.disconnect()
			this.oscillator.disconnect()
			this.panner.disconnect()
		}

	}

	function playChord(notes) {

	}

	function init() {

		this.oscillator.type = 'sawtooth';
		this.oscillator.connect(panner)
		this.panner.connect(gain)
		this.gain.connect(masterGain)
		this.gain.gain.value = 0.0

		this.oscillator.start(context.currentTime)

		return this

	}

	return {
		oscillator,
		panner,
		gain,
		playNote,
		init,
	}

}

export default Oscillator
