
import style from '../scss/style.scss'

// Shim for iOS.
import stereoPannerShim from 'stereo-panner-shim'

// Import modules.
import { context, masterGain, analyser } from './master.js'
import Clock from './clock.js'
import Transport from './transport.js'
import Notes from './notes.js'
import Chords from './chords.js'
import Sequencer from './sequencer.js'
import Delay from './delay.js'
import Canvas from './canvas.js'

// Clock

const clock = Clock({ 
	context: context, 
	onStepCallback: onStepEvent, 
	onStopCallback: onStop,
})

function onStepEvent({ currentStep, currentBeat, currentBar, tempo }) {
	sequencer.update(currentStep)
	transport.update({
		currentStep: clock.currentStep, 
		currentBeat: clock.currentBeat, 
		currentBar: clock.currentBar,
	})
}

function onStop() {
	// #TODO: update(0)
	sequencer.onStop()
	transport.update({
		currentStep: clock.currentStep, 
		currentBeat: clock.currentBeat, 
		currentBar: clock.currentBar,
	})
}

// Transport

const transport = Transport(clock)
transport.createHTML()
transport.update({
	currentStep: clock.currentStep, 
	currentBeat: clock.currentBeat, 
	currentBar: clock.currentBar, 
	tempo: clock.tempo,
})

// Sequencer

const sequencer = Sequencer('sequencer-1', context, masterGain)
sequencer.createHTML()

// Canvas

const canvas = Canvas(analyser)
canvas.create()


// Enable Web Audio API on iOS.

let iosEnabled = false

window.addEventListener('touchstart', function() {

	if(!iosEnabled) {

		// create empty buffer
		let buffer = context.createBuffer(1, 1, 22050)
		let source = context.createBufferSource()
		source.buffer = buffer

		// connect to output (your speakers)
		source.connect(context.destination)

		// play the file
		source.start(0)
		iosEnabled = true

	}

}, false)
