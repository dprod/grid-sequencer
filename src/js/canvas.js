

const Canvas = (audioAnalyser) => {

	let element = ''
	let canvas = ''
	let canvasContext = ''
	let canvasDebugger = ''

	let alpha = 1

	let analyser = audioAnalyser
	analyser.fftSize = 1024

	function create() {

		canvas = document.createElement('canvas')
		canvas.classList.add('canvas')
		document.body.appendChild(canvas)
		canvasContext = canvas.getContext('2d')

		canvas.width = 10
		canvas.height = 10

		this.draw()

	}

	function draw() {
		drawSpectrum(analyser, canvasContext)
		requestAnimationFrame(draw)
	}

	function getAverageVolume(array) {
		
		var values = 0
		var average

		var length = array.length

		for (var i = 0; i < length; i++)
			values += array[i]

		average = values / length
		return average

	}

	function drawSpectrum(analyser, canvasContext) {

		var width = canvasContext.canvas.width
		var height = canvasContext.canvas.height
		var bufferLength = analyser.frequencyBinCount
		var freqData = new Uint8Array(bufferLength)
		var dataArray = freqData
		var scaling = height / 256

		analyser.getByteFrequencyData(freqData)

		var average = getAverageVolume(dataArray)

		let alpha = Math.round((average/10)*100)/100

		let light = Math.round(average*5) * 1.5

		canvasContext.fillStyle = `rgba(${light+32}, ${light+42}, ${light+61}, 1)`
		canvasContext.fillRect(0, 0, width, height)

	}

	return {
		canvas,
		canvasContext,
		create,
		draw
	}


}

export default Canvas

