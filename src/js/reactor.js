function Reactor (object) {

	// Reactor
	// A Very Simple Reactive Object
	// Version 0.1.4

	for (let key in object) {

		if (object.hasOwnProperty(key)) {

			let val = object[key]

			Object.defineProperty(object, key, {
				get () {
					return val
				},
				set (newVal) {
					val = newVal
					object.onChange(key, val)
				}
			})

		}

	}

	return {
		data: object
	}

}

export default Reactor
