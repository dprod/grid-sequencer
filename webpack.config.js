
const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const DIST_DIR  = path.resolve(__dirname, 'dist/')
const SRC_DIR = path.resolve(__dirname, 'src/')

module.exports = {

	entry: SRC_DIR + '/js/app.js',

	output: {
		filename: 'app.js',
		path: DIST_DIR,
		publicPath: '/',
		// publicPath: path.resolve(__dirname, 'public/')
	},

	resolve: {
		extensions: ['.js', '.jsx']
	},

	devtool: 'source-map',

	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: SRC_DIR,
				// exclude: /node_modules/,
				query: {
					presets: ['es2015']
				}
			},
			{
				test: /\.scss$/,
				// use: ['css-hot-loader'].concat(ExtractTextPlugin.extract('style-loader!css-loader!sass-loader')),
				// loaders: ExtractTextPlugin.extract('css-loader!sass-loader'),
				loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap'],
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i, 
				loader: 'file-loader',
				include: SRC_DIR+'/images',
			}
		],
	},

	plugins: [
		new HtmlWebpackPlugin({
			title: 'sine-sequencer',
			template: SRC_DIR + '/index.html',
			filename: 'index.html',
			inject: 'body'
		}),
		new ExtractTextPlugin('style.css', {
			allChunks: true,
		})
	]

}